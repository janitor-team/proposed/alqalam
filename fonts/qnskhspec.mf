%%%%%%%%%%%%%%%%%%%%%%% qnskhspec.mf %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extended QNASKH Font for AlQalam
% special characters
% 
% 17 Shaban 1427, 10 Sep 2006
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (c) Hossam A. H. Fahmy
%
%    This work may be distributed and/or modified under the
%    conditions of the LaTeX Project Public License, either version 1.3
%    of this license or (at your option) any later version.
%    The latest version of this license is in
%      http://www.latex-project.org/lppl.txt
%    and version 1.3 or later is part of all distributions of LaTeX
%    version 2005/12/01 or later.
%     
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Hossam A. H. Fahmy.
% (hfahmy@arith.stanford.edu)
%
% This work consists of all the files listed in alqalam_manifest.txt.
%
%     
%      Based on the original work of the arabtex package.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if unknown qnskhbase: input qnskhbase fi;

%%%%%%%%%%%%%%%%%%%%  Quran 2006 Project  %%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%    very special characters     %%%%%%%%%%%%%%%%%

arabchar(space_spec, f_beg, 5, 8, 6);
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(space_spec, f_mid, 0, 8, 6);
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(tatwil, f_iso, 5, 5, 0);
draw z.r--z.l;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(tatwil, f_beg, 5, 5, 0);
draw z.r--z.l;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(tatwil, f_mid, 5, 5, 0); 
draw z.r--z.l;
endchar; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
arabchar(tatwil, f_end, 5, 5, 0);
draw z.r--z.l;
endchar;                                                                                     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(block, f_iso, 10, 12, 0); % block isoliert
x1 = x2 = x.l + dx; x3 = x4 = x.r - dx;
y1 = y4 = 10dy; y2 = y3 = 0;
fill z1--z2--z3--z4--cycle;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(block, f_beg, 10, 12, 0); % block initial
x1 = x2 = x.l + dx; x3 = x4 = x.m;
y1 = y4 = 10dy; y2 = y3 = 0;
fill z1--z2--z3--z4--cycle;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(block, f_mid, 10, 12, 0); % block medial
x1 = x2 = x.l + dx; x3 = x4 = x.r - dx;
y1 = y4 = 5dy; y2 = y3 = 0;
fill z1--z2--z3--z4--cycle;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(block, f_end, 10, 12, 0); % block final
x1 = x2 = x.m; x3 = x4 = x.r - dx;
%x1 = x2 = x.l + dx; x3 = x4 = x.r - dx;
y1 = y4 = 10dy; y2 = y3 = 0;
fill z1--z2--z3--z4--cycle;
endchar; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%     special characters     %%%%%%%%%%%%%%%%%%%
arabchar(".", 0, 2, 2, -1.5);
x1 = x.m; y1 = dy;
put_dot(1);
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(",", 0, 4, 10, 0);
x1 = x2 = x.m; y1 = 6dy; y2 = dy;
%x1 = x2 = x.m; y1 = 10dy; y2 = 5dy;
draw z1{(-dx,-dy)}..z2{right};
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(";", 0, 4, 10, 0);
x1 = x2 = x.m; y1 = 10dy; y2 = 5dy;
draw z1{(-dx,-dy)}..z2{right}; 
one_dot(1);
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(":", 0, 4, 10, 0);
x1 = x2 = x.m; y1 = dy; y2 = 6dy;
put_dot(1); put_dot(2);
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("!", 0, 4, 15, 0);
x1 = x2 = x3 = x.m; y1 = dy; y2 = 4dy; y3 = 13dy;
%x2a - x2 = x2 - x2b = .5dx; x3a - x3 = x3 - x3b = dx;
x2a - x2 = x2 - x2b = .3px; x3a - x3 = x3 - x3b = .6px;
y2a = y2b = y2; y3a = y3b = y3;
put_dot(1); 
fill z2a--z3a--z3b--z2b--cycle;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("?", 0, 7, 15, 0);
x1 - 2dx = x3 + 2dx = x2 = x4 = x.m;
y1 = y3 = 10dy; y2 = 13dy; y4 = 4dy;
draw z1..z2{left}..z3..z4{(-4dx,-11dy)};
one_dot(1);
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("$", 0, 6, 10, 5);
%x1 = x2 = x.m; x3 = x5 = x.m + 2dx; x4 = x6 = x.m - 2dx;
x1 = x2 = x.m; x3 = x5 = x.m + 3dx; x4 = x6 = x.m - 3dx;
y1 = 8dy; y2 = -6dy; y3 = y4 = 3dy; y5 = y6 = -dy;
draw z1--z2; draw z3..z4..z5..z6;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("*", 0, 8, 10, 0);
x1 = x2 = x.m; x3 = x5 = x.m + 3dx; x4 = x6 = x.m - 3dx;
y1 = 7dy; y3 = y6 = 5dy; y4 = y5 = dy; y2 = -dy;
draw z1--z2; draw z3--z4; draw z5--z6;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("%", 0, 14, 10, 5);
x1 = x5 = x6 = x.m + 4dx;
x2 = x3 = x4 = x.m - 4dx;
y1 = y3 = 8dy; y4 = 3dy; y5 = dy; y2 = y6 = -4dy;
draw z3..z4..cycle; draw z5..z6..cycle;
pickup dia_pen; draw z1--z2; 
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("/", 0, 10, 10, 5);
pickup dia_pen;
x1 + 4dx = x2 - 4dx = x.m;
y2 = 9dy; y1 = -6dy;
draw z1--z2;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(rquotes, 0, 8, 10, 5); % ``
x1 = x3 = x2 + 2dx = x.m - dx;
x4 = x6 = x5 + 2dx = x.m + 3dx;
y1 = y4 = 5dy; y2 = y5 = dy; y3 = y6 = -3dy;
draw z1..z2..z3; draw z4..z5..z6;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(lquotes, 0, 8, 10, 5); % ''
x1 = x3 = x2 - 2dx = x.m + dx;
x4 = x6 = x5 - 2dx = x.m - 3dx;
y1 = y4 = 5dy; y2 = y5 = dy; y3 = y6 = -3dy;
draw z1..z2..z3; draw z4..z5..z6;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("=", 0, 16, 10, 0);
x5 = x7 = x.m - 7dx; x6 = x8 = x.m + 7dx;
y5 = y6 = 3dy; y7 = y8 = -dy;
draw z5--z6; draw z7--z8;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("+", 0, 10, 10, 0);
x1 = x2 = x3 + 4dx = x4 - 4dx = x.m;
y1 = 5dy; y2 = -3dy; y3 = y4 = dy;
draw z1--z2; draw z3--z4;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("-", 0, 8, 10, 0);
x1 + 3dx = x2 - 3dx = x.m;
y1 = y2 = dy;
%y1 = y2 = 4dy;
draw z1--z2;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("#", 0, 16, 10, 5);
x1 = x4 = x3 - 4dx = x2 + 4dx = x.m;
x5 = x7 = x.m - 7dx; x6 = x8 = x.m + 7dx;
y1 = y3 = 7dy; y2 = y4 = -7dy;
y5 = y6 = 2dy; y7 = y8 = -2dy;
draw z5--z6; draw z7--z8;
pickup dia_pen;
draw z1--z2; draw z3--z4;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(")", 0, 8, 10, 5);
x1 = x2 = x.m - 3dx; x3 = x.m + 3dx;
top y1 = 10dy; bot y2 = -7dy; y3 = .5[y1,y2];
draw z1..z3..z2;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("(", 0, 8, 10, 5);
x1 = x2 = x.m + 3dx; x3 = x.m - 3dx;
top y1 = 10dy; bot y2 = -7dy; y3 = .5[y1,y2];
%top y1 = 15dy; bot y2 = -7dy; y3 = 4dy;
draw z1..z3..z2;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("]", f_iso, 20, 19, 9);
%arabchar(quran_17_openarc, f_iso, 20, 19, 9);
pickup pencircle xscaled .5pt yscaled .5pt rotated 0;

z13=z.m+(0,4dx);
e=.333;
f=2;
z12=z13+(0,.15c);
z3=z13+(.15c,0);
z6=z13-(0,.15c);
z9=z13-(.15c,0);

z1=e[z12,z3];
z2=e[z3,z12];
z4=e[z3,z6];
z5=e[z6,z3];
z7=e[z6,z9];
z8=e[z9,z6];
z10=e[z9,z12];
z11=e[z12,z9];

z1.l=z1+(f*dx,f*dx);
z2.l=z2+(f*dx,f*dx);
z4.l=z4+(f*dx,-f*dx);
z5.l=z5+(f*dx,-f*dx);
z7.l=z7+(-f*dx,-f*dx);
z8.l=z8+(-f*dx,-f*dx);
z10.l=z10+(-f*dx,f*dx);
z11.l=z11+(-f*dx,f*dx);

z3.l=z3+(2f*dx,0);
z9.r=z9-(2f*dx,0);

z14=.25[z1.l,z11.l];
z15=.25[z11.l,z1.l];

z16=.25[z5.l,z7.l];
z17=.25[z7.l,z5.l];

z18=z13+(-7dx,15dy);
z19=z13+(-7dx,-15dy);

draw z18..z14{down}--z1.l--z1;
draw z18..z15{down}--z11.l--z11;

draw z19..z16{up}--z5.l--z5;
draw z19..z17{up}--z7.l--z7;

draw z10--z10.l..z9.r{dir 210};draw z8--z8.l..z9.r{dir 150};
draw z2--z2.l..z3.l{dir -30};draw z4--z4.l..z3.l{dir 30};

draw z12--z3--z6--z9--z12;

endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar("[", f_iso, 20, 19, 9);
pickup pencircle xscaled .5pt yscaled .5pt rotated 0;
z13=z.m+(0,4dx);
%e=.333;
%f=2;
z12=z13+(0,.15c);
z3=z13+(.15c,0);
z6=z13-(0,.15c);
z9=z13-(.15c,0);

z1=e[z12,z3];
z2=e[z3,z12];
z4=e[z3,z6];
z5=e[z6,z3];
z7=e[z6,z9];
z8=e[z9,z6];
z10=e[z9,z12];
z11=e[z12,z9];

z1.l=z1+(f*dx,f*dx);
z2.l=z2+(f*dx,f*dx);
z4.l=z4+(f*dx,-f*dx);
z5.l=z5+(f*dx,-f*dx);
z7.l=z7+(-f*dx,-f*dx);
z8.l=z8+(-f*dx,-f*dx);
z10.l=z10+(-f*dx,f*dx);
z11.l=z11+(-f*dx,f*dx);

z3.l=z3+(2f*dx,0);
z9.r=z9-(2f*dx,0);

z14=.25[z1.l,z11.l];
z15=.25[z11.l,z1.l];

z16=.25[z5.l,z7.l];
z17=.25[z7.l,z5.l];

z18=z13+(7dx,15dy);
z19=z13+(7dx,-15dy);

draw z18..z14{down}--z1.l--z1;
draw z18..z15{down}--z11.l--z11;
draw z19..z16{up}--z5.l--z5;
draw z19..z17{up}--z7.l--z7;
draw z10--z10.l..z9.r{dir 210};draw z8--z8.l..z9.r{dir 150};
draw z2--z2.l..z3.l{dir -30};draw z4--z4.l..z3.l{dir 30};
draw z12--z3--z6--z9--z12;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%arabchar("[", 0, 6, 10, 5);
%x1 = x2 = x.m + 2dx; x3 = x4 = x.m - 2dx;
%y1 = y3 = 9dy; y2 = y4 = -6dy;
%y1 = y3 = 14dy; y2 = y4 = -6dy;
%draw z1--z3--z4--z2;
%endchar;

%arabchar("]", 0, 6, 10, 5);
%x1 = x2 = x.m + 2dx; x3 = x4 = x.m - 2dx;
%y1 = y3 = 9dy; y2 = y4 = -6dy;
%draw z3--z1--z2--z4;
%endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(quran_19_openarc, 0, 8, 10, 5); % oct 216 , 217 for [ and ]
x1 = x2 = x.m + 2dx; x3 = x4 = x.m - 2dx;
y1 = y3 = 9dy; y2 = y4 = -6dy;
draw z1--z3--z4--z2;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
arabchar(quran_20_closearc, 0, 8, 10, 5);
x1 = x2 = x.m + 2dx; x3 = x4 = x.m - 2dx;
y1 = y3 = 9dy; y2 = y4 = -6dy;
draw z3--z1--z2--z4;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
endinput;
%%%%%%%%%%%%%%%%%%%%%%%%%     EOF     %%%%%%%%%%%%%%%%%%%%%%%%%

